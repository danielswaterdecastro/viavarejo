import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { EnvironmentType } from 'vv-jornada-core-ngx/dist/environment';
import { environment } from './environments/environment';

if (
  environment.type === EnvironmentType.DOCKER ||
  environment.type === EnvironmentType.DESENVOLVIMENTO
) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
