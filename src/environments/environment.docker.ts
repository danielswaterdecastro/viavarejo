// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { EnvironmentPadrao, EnvironmentType } from 'vv-jornada-core-ngx/dist/environment';
import { Bandeira } from 'vv-jornada-core-ngx/dist/common';
import { Aplicacao } from 'vv-jornada-core-ngx/dist/integracao';

class EnvironmentDocker implements EnvironmentPadrao {

  type = EnvironmentType.DOCKER;
  server = {
    [Bandeira.CASAS_BAHIA]: '${API_BASE_URL_CB}',
    [Bandeira.PONTOFRIO]: '${API_BASE_URL_PF}'
  };
  aplicacao = Aplicacao.LISTA_CASAMENTO;

}

export const environment = new EnvironmentDocker();
