import { Aplicacao } from 'vv-jornada-core-ngx/dist/integracao/contratos/aplicacao.enum';
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { EnvironmentPadrao, EnvironmentType } from 'vv-jornada-core-ngx/dist/environment';

class EnvironmentDev implements EnvironmentPadrao {

  type = EnvironmentType.DESENVOLVIMENTO;
  // server = 'http://10.128.132.217';
  server = 'http://casasbahia.cwi-1.viavarejo.local';
  // server = 'http://vv-zuulserver-dev.jornada.dc.nova';
  // server = 'http://10.99.50.85:8083';
  sessao = null;
  aplicacao: Aplicacao = Aplicacao.LISTA_CASAMENTO;

}

export const environment = new EnvironmentDev();
