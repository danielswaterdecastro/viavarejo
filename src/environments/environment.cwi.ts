// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { Aplicacao } from 'vv-jornada-core-ngx/dist/integracao/contratos/aplicacao.enum';
import { EnvironmentPadrao, EnvironmentType } from 'vv-jornada-core-ngx/dist/environment';

class EnvironmentCwi implements EnvironmentPadrao {

  type = EnvironmentType.DESENVOLVIMENTO;
  server = '${API_BASE_URL_CB}';
  sessao = null;
  aplicacao: Aplicacao = Aplicacao.LISTA_CASAMENTO;

}

export const environment = new EnvironmentCwi();
