import { CadastroListaCasamentoService } from './cadastro/shared/services/cadastro-lista-casamento.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { UtilModule } from 'vv-jornada-core-ngx/dist/util';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { EnvironmentService } from 'vv-jornada-core-ngx/dist/environment';

import { IntegracaoModule } from 'vv-jornada-core-ngx/dist/integracao';
import { HeaderModule } from 'vv-jornada-core-ngx/dist/header/header.module';
import { HttpBuilderModule } from 'vv-jornada-core-ngx/dist/http-builder';
import { CadastroGuardService } from './cadastro/shared/services/cadastro-guard.service';
import { StepControlService } from './cadastro/shared/services/step-control.service';
import { CadastroModule } from './cadastro/cadastro.module';
import {
  CadastroTelaSucessoComponent
} from './cadastro/scenes/cadastro-confirmacao/cadastro-tela-sucesso/cadastro-tela-sucesso.component';
import { BuscaDescricaoModule } from 'vv-jornada-core-ngx/dist/busca-descricao';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    CadastroTelaSucessoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    IntegracaoModule.forRoot(),
    HttpBuilderModule.forRoot(),
    UtilModule.forRoot(),
    HeaderModule,
    CadastroModule,
    BuscaDescricaoModule,
    SharedModule,
  ],
  providers: [
    StepControlService,
    CadastroGuardService,
    CadastroListaCasamentoService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }

EnvironmentService.environment = environment;
