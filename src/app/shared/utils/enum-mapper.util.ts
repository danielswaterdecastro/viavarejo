export class EnumMapper {

  public static toKeys<T>(source: T): Map<string, number> {

    const map = new Map<string, number>();
    const hash = Object.entries(source);

    for (let i = 0, len = hash.length; i < len; i++) {
      map.set(hash[i][1], <any>i);
    }

    return map;

  }

  public static toValues<T>(source: T): Map<string, string> {

    const map = new Map<string, string>();

    for (const [key, value] of Object.entries(source)) {
      map.set(value, key);
    }

    return map;
  }

}
