import { EnumTransform } from './enum-transform.util';
import { OrderBy } from '../../cadastro/scenes/cadastro-selecao-lista-detalhe/enums/order-by.enum';

describe('EnumTransform', () => {

  it('should convert enum to array', () => {

    expect(EnumTransform.toArray(OrderBy)).toEqual([
      'Relevância',
      'Mais vendidos',
      'Alfabética'
    ]);

  });

});
