import { OrderBy } from '../../cadastro/scenes/cadastro-selecao-lista-detalhe/enums/order-by.enum';
import { EnumMapper } from './enum-mapper.util';

describe('EnumMapper', () => {

  it('should map enum to keys', () => {
    const map = EnumMapper.toKeys(OrderBy);
    expect(map.get(OrderBy.RELEVANCIA)).toBe(0);
  });

  it('should map enum to values', () => {
    const map = EnumMapper.toValues(OrderBy);
    expect(map.get(OrderBy.RELEVANCIA)).toMatch(/RELEVANCIA/);
  });

});
