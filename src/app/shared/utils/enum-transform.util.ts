export class EnumTransform {

  public static toArray<T>(source: T): Array<string> {

    const arr = new Array<string>();
    for (const [, value] of Object.entries(source)) {
      arr.push(value);
    }
    return arr;

  }

}
