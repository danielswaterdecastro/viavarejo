import { Departamento } from './departamento.model';
import { IListaSugestiva } from '../interfaces/lista-sugestiva.interface';
import { IProduto } from '../interfaces/produto.interface';

export class ListaSugestiva implements IListaSugestiva {
  public codigo: number;
  public nome: string;
  public produtos: Array<IProduto>;
  public quantidadeTotalItens: number;
  public valorTotalAproximado: number;
}
