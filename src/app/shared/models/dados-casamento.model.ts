import { IDadosCasamento } from './../interfaces/dados-casamento.interface';

export class DadosCasamento implements IDadosCasamento {
  public cidadeEstado: string;
  public data: string;
  public emailNoivo: string;
  public nomeNoivo: string;
  public nomeOutroNoivo: string;
}
