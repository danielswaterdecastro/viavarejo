export class Municipio {
  codigo: number;
  nome: string;
  estado: {
    nome: string;
    UF: string
  };
}
