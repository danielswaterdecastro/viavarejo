import { IClienteAtendimento } from './../interfaces/cliente-atendimento.interface';

export class ClienteAtendimento implements IClienteAtendimento {
  public codigo: number;
  public possuiCadastroSite: boolean;
}
