import { IClienteAtendimento } from './../interfaces/cliente-atendimento.interface';
import { IListaCasamento } from './../interfaces/lista-casamento.interface';
import { IDadosCasamento } from '../interfaces/dados-casamento.interface';
import { IListaSugestiva } from '../interfaces/lista-sugestiva.interface';

export class ListaCasamento implements IListaCasamento {
  public codigo: number;
  public clienteAtendimento: IClienteAtendimento;
  public dados: IDadosCasamento;
  public listaSugestiva: IListaSugestiva;
  public codigoListaCompra: number;
}
