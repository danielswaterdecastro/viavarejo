import { ModalService } from 'vv-jornada-core-ngx/dist/modal/modal.service';
import { Municipio } from '../models/municipio.model';
import { Injectable } from '@angular/core';
import { HttpBuilder } from 'vv-jornada-core-ngx/dist/http-builder';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MunicipioService {

  private municipioSource = new BehaviorSubject<Municipio[]>([]);
  public municipio$ = this.municipioSource.asObservable();

  constructor(private httpBuild: HttpBuilder, private modalService: ModalService) { }

  buscarMunicipio(textoBusca: string): void {
    if (!!textoBusca) {
      this.busca(textoBusca).subscribe(municipios => {
        this.municipioSource.next(municipios);
      },
      erro => {
        this.modalService.criarAlertaErro(erro);
      });
    }
  }

  busca(termo: string): Observable<Municipio[]> {
    return this.httpBuild.get(`vv-crm/enderecos/estados/municipios?q=${termo}&page=0&per_page=10`)
      .token()
      .build<Array<Municipio>>();
  }
}
