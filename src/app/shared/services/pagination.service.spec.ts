import { PaginationService } from './pagination.service';

describe('PaginationService', () => {

  let paginationService: PaginationService;

  beforeEach(() => { paginationService = new PaginationService(); });

  describe('should test service creation', () => {

    it('should be created', () => {
      expect(paginationService).toBeTruthy();
    });

  });

  describe('should test observed getter and setters props', () => {

    it('should set and get observed page',
      (done: DoneFn) => {

        paginationService.page = 1;

        paginationService.page$.subscribe((value: number) => {
          expect(value).toBe(1);
          done();
        });

      });

    it('should set and get observed max',
      (done: DoneFn) => {

        paginationService.max = 9;

        paginationService.max$.subscribe((value: number) => {
          expect(value).toBe(9);
          done();
        });

      });

    it('should set and get observed offset',
      (done: DoneFn) => {

        paginationService.offset = 1;

        paginationService.offset$.subscribe((value: number) => {
          expect(value).toBe(1);
          done();
        });

      });

  });

  describe('should navigate through pagination', () => {

    it('should increment pagination step', () => {
      paginationService.page = 1;
      paginationService.next();
      expect(paginationService.page).toBe(2);
    });

    it('should decrement pagination step', () => {
      paginationService.page = 4;
      paginationService.prev();
      expect(paginationService.page).toBe(3);
    });

  });

  describe('should validate permission to move back and forwards', () => {

    let permission: boolean;

    beforeEach(() => {
      paginationService.page = 1;
      paginationService.offset = 9;
      paginationService.calcMaxPage(30);
    });

    it('should hide pagination', () => {
      paginationService.max = 1;
      permission = paginationService.shouldShowPagination();
      expect(permission).toBeFalsy();
    });

    it('should show pagination', () => {
      permission = paginationService.shouldShowPagination();
      expect(permission).toBeTruthy();
    });

    it('should calc max page', () => {
      expect(paginationService.max).toBe(4);
    });

    it('should hide prev button', () => {
      permission = paginationService.shouldShowPrev(paginationService.page);
      expect(permission).toBeFalsy();
    });

    it('should show prev button', () => {
      paginationService.next();
      permission = paginationService.shouldShowPrev(paginationService.page);
      expect(permission).toBeTruthy();
    });

    it('should hide next button', () => {
      paginationService.page = 4;
      permission = paginationService.shouldShowNext(paginationService.page);
      expect(permission).toBeFalsy();
    });

    it('should show next button', () => {
      permission = paginationService.shouldShowNext(paginationService.page);
      expect(permission).toBeTruthy();
    });

  });

});
