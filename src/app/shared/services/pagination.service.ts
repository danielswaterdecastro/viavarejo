import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class PaginationService {

  private pageSource = new BehaviorSubject<number>(0);
  public page$ = this.pageSource.asObservable();

  private maxSource = new BehaviorSubject<number>(0);
  public max$ = this.maxSource.asObservable();

  private offsetSource = new BehaviorSubject<number>(0);
  public offset$ = this.offsetSource.asObservable();

  get page(): number {
    return this.pageSource.getValue();
  }

  set page(page: number) {
    this.pageSource.next(page);
  }

  get max(): number {
    return this.maxSource.getValue();
  }

  set max(max: number) {
    this.maxSource.next(max);
  }

  get offset(): number {
    return this.offsetSource.getValue();
  }

  set offset(offset: number) {
    this.offsetSource.next(offset);
  }

  public next(): void {
    this.page = (this.page + 1);
  }

  public prev(): void {
    this.page = (this.page - 1);
  }

  public shouldShowPrev(page: number): boolean {
    return this.page > 1;
  }

  public shouldShowNext(page: number): boolean {
    return this.page < this.max;
  }

  public shouldShowPagination(): boolean {
    return this.max !== 1;
  }

  public calcMaxPage(totalItems: number): void {
    this.max = Math.ceil(totalItems / this.offset) || 1;
  }

}
