import { Injectable, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { PessoaTipo } from 'vv-jornada-core-ngx/dist/cliente';
import { NomeSocialPipe } from 'vv-jornada-core-ngx/dist/cliente/nome-social.pipe';
import { Aplicacao } from 'vv-jornada-core-ngx/dist/integracao';
import { RoteadorService } from 'vv-jornada-core-ngx/dist/integracao/roteador/roteador.service';
import { ModalService } from 'vv-jornada-core-ngx/dist/modal/modal.service';
import { Cliente, IniciarAtendimentoFluxo } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';
import { AtendimentoAtivoService } from 'vv-jornada-core-ngx/dist/solicitar-atendimento/atendimento-ativo.service';
import { SolicitarAtendimentoService } from 'vv-jornada-core-ngx/dist/solicitar-atendimento/solicitar-atendimento.service';
import { CadastroRouteKeys } from '../../cadastro/shared/enums/cadastro-route-keys.enum';
import { StepControlService } from '../../cadastro/shared/services/step-control.service';
import { CadastroListaCasamentoService } from './../../cadastro/shared/services/cadastro-lista-casamento.service';
import { ClienteAtendimento } from './../models/cliente-atendimento.model';
import { DadosCasamento } from './../models/dados-casamento.model';
import { ListaCasamentoRestService } from './lista-casamento-rest.service';

@Injectable()
export class ListaCasamentoService implements OnDestroy {

  private subscriptions = new Subscription();

  constructor(
    private solicitarAtendimentoService: SolicitarAtendimentoService,
    private router: Router,
    private listaCasamentoRestService: ListaCasamentoRestService,
    private modalService: ModalService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private stepControlService: StepControlService,
    private atendimentoAtivoService: AtendimentoAtivoService,
    private roteador: RoteadorService
  ) { }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  private clientePossuiCadastroSite(dadosClienteAtendimento): void {
    const subscription = this.listaCasamentoRestService.clienteCadastradoSite(dadosClienteAtendimento.codigo)
      .subscribe((dadosClienteSite: ClienteAtendimento) => {
        const clienteAtendimento = new ClienteAtendimento();

        clienteAtendimento.possuiCadastroSite = !!dadosClienteSite;
        clienteAtendimento.codigo = dadosClienteAtendimento.codigo;

        this.cadastroListaCasamentoService.cliente = clienteAtendimento;

        const nomeSocialPipe = new NomeSocialPipe();
        const nomeCliente = <string>nomeSocialPipe.transform(dadosClienteAtendimento);

        if (this.existeCadastroListaCasamentoDados()) {
          this.cadastroListaCasamentoService.dados.nomeNoivo = nomeCliente;
        } else {
          const dadosCasamento = new DadosCasamento();

          dadosCasamento.nomeNoivo = nomeCliente;

          this.cadastroListaCasamentoService.dados = dadosCasamento;
        }

        this.redirect();
      },
        erro => {
          const originalErro = erro;
          if (!!erro.informacoesTecnicas._body) {
            erro = JSON.parse(erro.informacoesTecnicas._body);
          }

          if ('erros' in erro && erro.erros.some(e => e.codigo === 1)) {
            this.abrirModalDadosIncorretos(dadosClienteAtendimento);
          } else {
            this.modalService.criarAlertaErro(originalErro);
          }
        });

    this.subscriptions.add(subscription);
  }

  private existeCadastroListaCasamentoDados() {
    return !!this.cadastroListaCasamentoService.dados;
  }

  private redirect() {
    this.stepControlService.toStep(CadastroRouteKeys.DADOS_CASAMENTO);
  }

  private solicitarAtendimentoClienteSite(): void {
    const modalAtendimento = this.solicitarAtendimentoService.abrirModalAtendimento(IniciarAtendimentoFluxo.IDENTIFICAR_PF)
      .subscribe((dadosAtendimento) => {
        this.clientePossuiCadastroSite(dadosAtendimento.cliente);
      });

    const subscription = this.modalService.fecharModal.subscribe(() => modalAtendimento.unsubscribe());
    this.subscriptions.add(subscription);
  }

  /**
   * @todo Criar teste unitário
   */
  public async criarListaCasamento() {
    const atendimento = await this.atendimentoAtivoService.atendimento;

    if (atendimento && atendimento.cliente.codigo && atendimento.cliente.tipoPessoa === PessoaTipo.PESSOA_FISICA) {
      this.clientePossuiCadastroSite(atendimento.cliente);
    } else {
      this.solicitarAtendimentoClienteSite();
    }
  }

  /* Verificar qual o comportamento correto para se abrir essa modal. Verificar com a Andreia */
  private abrirModalDadosIncorretos(cliente: Cliente) {

    const nomeCliente = !cliente.nome ? '' : cliente.nome;
    const mensagem = `<p>O cadastro do cliente <strong class="is-error">${nomeCliente}</strong> está incompleto.
       Antes de continuar a criação da lista de casamento, é necessário completar os campos obrigatórios.</p>
      <p>Deseja atualizar agora?</p>`;

    this.modalService
      .criarConfirmador(mensagem)
      .subscribe(retorno => {
        if (retorno) {
          this.roteador.abrir(Aplicacao.CRM, `cliente/${cliente.tipoPessoa}/${cliente.codigo}`);
        }
      });
  }
}
