import { ListaCasamento } from './../models/lista-casamento.model';
import { IClienteAtendimento } from './../interfaces/cliente-atendimento.interface';
import { Endpoints } from './../../endpoints/endpoints';
import { ListaSugestiva } from './../../shared/models/lista-sugestiva.model';
import { Observable } from 'rxjs/Observable';
import { Injectable, OnDestroy } from '@angular/core';
import { HttpBuilder } from 'vv-jornada-core-ngx/dist/http-builder';
import { Departamento } from '../../shared/models/departamento.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { OrderByMap } from '../../cadastro/scenes/cadastro-selecao-lista-detalhe/maps/order-by.map';
import { OrderBy } from '../../cadastro/scenes/cadastro-selecao-lista-detalhe/enums/order-by.enum';
import { IProduto } from '../interfaces/produto.interface';
import { IRequestListaSugestiva } from '../interfaces/request-lista-sugestiva.interface';
import { ModalService } from 'vv-jornada-core-ngx/dist/modal';

@Injectable()
export class ListaCasamentoRestService implements OnDestroy {

  private subscriptions = new Subscription();

  private listaSugestivaSource = new BehaviorSubject<ListaSugestiva>(new ListaSugestiva);
  public listaSugestiva$ = this.listaSugestivaSource.asObservable();

  private produtosSource = new BehaviorSubject<Array<IProduto>>(new Array<IProduto>());
  public produtos$ = this.produtosSource.asObservable();

  private departamentoSource = new BehaviorSubject<Departamento[]>([]);
  public departamento$ = this.departamentoSource.asObservable();

  constructor(
    private httpBuild: HttpBuilder,
    private modalService: ModalService
  ) { }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  public buscarListasSugestivas(): Observable<ListaSugestiva[]> {
    return this.httpBuild
      .get(Endpoints.listaCasamento.buscarListasSugestivas())
      .token()
      .build<ListaSugestiva[]>();
  }

  private departamentosListaSugestiva(codigo: number): Observable<Departamento[]> {
    return this.httpBuild
      .get(Endpoints.listaCasamento.departamentosListaSugestiva(codigo))
      .token()
      .build<Departamento[]>();
  }

  private detalhesListaSugestiva(params: IRequestListaSugestiva): Observable<ListaSugestiva> {

    const query = this.resolveDepartamento(params);

    return this.httpBuild
      .get(Endpoints.listaCasamento.detalhesListaSugestiva(params.codigo))
      .setQueryParams(query)
      .token()
      .build<ListaSugestiva>();

  }

  private resolveDepartamento(params: IRequestListaSugestiva): Object {
    const { codigo, departamento, ...props } = params;
    return departamento ? { departamento, ...props } : props;
  }

  public buscarDetalhesListaSugestiva(
    codigo: number,
    departamento: number,
    ordenacao: string = OrderByMap.get(OrderBy.RELEVANCIA),
    per_page: number = 1,
    page: number = 1
  ): void {

    const subscription = this.detalhesListaSugestiva({ codigo, departamento, ordenacao, per_page, page })
      .subscribe((listasSugestivas: ListaSugestiva) => {
        this.produtosSource.next(listasSugestivas.produtos);
        this.listaSugestivaSource.next(listasSugestivas);
      }, erro => this.modalService.criarAlertaErro(erro));

    this.subscriptions.add(subscription);

  }

  public buscarDepartamentos(codigoListaSugestiva: number): void {

    const subscription = this.departamentosListaSugestiva(codigoListaSugestiva)
      .subscribe((departamentos: Departamento[]) => {
        this.departamentoSource.next(departamentos);
      }, erro => this.modalService.criarAlertaErro(erro));

    this.subscriptions.add(subscription);
  }

  public clienteCadastradoSite(codigoCliente: number) {
    return this.httpBuild
      .get(Endpoints.listaCasamento.clienteSite(codigoCliente))
      .token()
      .build<IClienteAtendimento>();
  }

  public cadastrarListaCasamento(listaCasamento: ListaCasamento): Observable<ListaCasamento> {
    return this.httpBuild
      .post(Endpoints.listaCasamento.cadastrarListaCasamento())
      .setBodyParams(listaCasamento)
      .token()
      .build<ListaCasamento>();
  }

  public clearProducts(): void {
    this.produtosSource.next(new Array<IProduto>());
  }

  public clearSuggestiveList(): void {
    this.listaSugestivaSource.next(new ListaSugestiva());
  }

  public clearDepartment(): void {
    this.departamentoSource.next(new Array());
  }

  public clearAllProps(): void {
    this.clearProducts();
    this.clearSuggestiveList();
    this.clearDepartment();
  }

}
