
import { HttpBuilderModule } from 'vv-jornada-core-ngx/dist/http-builder';
import { TestBed, inject } from '@angular/core/testing';

import { ListaCasamentoRestService } from './lista-casamento-rest.service';
import { IntegracaoModule } from 'vv-jornada-core-ngx/dist/integracao';
import { ModalModule } from 'vv-jornada-core-ngx/dist/modal';

describe('ListaCasamentoRestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpBuilderModule.forRoot(),
        IntegracaoModule.forRoot(),
        ModalModule.forRoot(),
      ],
      providers: [
        ListaCasamentoRestService
      ]
    });
  });

  it('should be created', inject([ListaCasamentoRestService], (service: ListaCasamentoRestService) => {
    expect(service).toBeTruthy();
  }));
});
