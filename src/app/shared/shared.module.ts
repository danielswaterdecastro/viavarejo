import { ListaCasamentoRestService } from './services/lista-casamento-rest.service';
import { ListaCasamentoService } from './services/lista-casamento.service';
import { NgModule } from '@angular/core';

import { DefaultTemplateComponent } from './components/default-template/default-template.component';
import { IconComponent } from './components/icon/icon.component';
import { AutocompleteMunicipioComponent } from './components/autocomplete-municipio/autocomplete-municipio.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

const COMPONENTS = [
  DefaultTemplateComponent,
  IconComponent,
  AutocompleteMunicipioComponent,
];

@NgModule({
  declarations: [COMPONENTS],
  exports: [COMPONENTS],
  providers: [
    ListaCasamentoService,
    ListaCasamentoRestService
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})

export class SharedModule { }
