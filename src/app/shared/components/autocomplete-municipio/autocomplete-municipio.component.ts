import { Component, Input, Output, OnDestroy, EventEmitter, OnInit } from '@angular/core';
import { MunicipioService } from './../../services/municipio.service';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { debounceTime } from 'rxjs/operators';
import { Municipio } from './../../models/municipio.model';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'vnd-autocomplete-municipio',
  template: `
  <div class="float-label">
    <input
      class="no-optional-text"
      id="municipio"
      type="text"
      placeholder="Cidade"
      [ngModel]="municipioInformado"
      [class.has-value]="municipioInformado"
      [class.has-error]="error"
      (keyup)="updateTermoBusca($event.target.value)"
      autocomplete="off">
    <label for="municipio">Cidade *</label>
    <span class="error-msg" role="alert" *ngIf="error">Campo obrigatório</span>

    <div class="autocomplete" *ngIf="!!(textoBusca$ | async)">

      <div class="options-list">
        <ul>
          <li *ngFor="let municipio of municioService.municipio$ | async"
          (click)="selecionaMunicipio(municipio.nome + ' - ' + municipio.estado.UF)">
            <a ng-href >{{municipio.nome}} - {{municipio.estado.UF}}</a>
          </li>
          <li *ngIf="!(municioService.municipio$ | async)">
            <p>Município não encontrado.</p>
          </li>
        </ul>
      </div>

    </div>
  </div>
`,
  styleUrls: ['autocomplete-municipio.component.scss'],
  providers: [MunicipioService],
})
export class AutocompleteMunicipioComponent implements OnInit, OnDestroy {

  textoBusca$ = new Subject<string>();
  subscriptionTextoBusca: Subscription;

  @Output() municipioOutput = new EventEmitter<string>();
  @Input() municipioInformado: string;
  @Input() error: boolean;

  constructor(public municioService: MunicipioService) { }

  ngOnInit(): void {
    this.subscriptionTextoBusca =
      this.textoBusca$
        .debounceTime(500)
        .distinctUntilChanged()
        .subscribe(termo => this.municioService.buscarMunicipio(termo));
  }

  ngOnDestroy(): void {
    this.subscriptionTextoBusca.unsubscribe();
  }

  updateTermoBusca(termo: string): void {
    if (this.municipioInformado !== termo) {
      this.municipioInformado = termo;
      if (!termo) {
        this.municipioOutput.emit('');
      }
      this.textoBusca$.next(termo);
    }
  }

  selecionaMunicipio(municipio: string): void {
    this.textoBusca$.next('');
    this.municipioInformado = municipio;
    this.municipioOutput.emit(municipio);
  }

}
