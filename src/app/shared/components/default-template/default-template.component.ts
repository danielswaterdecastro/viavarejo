import { Component } from '@angular/core';

@Component({
  selector: 'vnd-default-template',
  template: `
        <div class="default-template">
          <header class="wrapper">
            <ng-content select="[header]"></ng-content>
          </header>
          <section class="wrapper">
              <ng-content select="[content-strech-before]"></ng-content>
              <div class="content-template">
                <ng-content></ng-content>
              </div>
              <ng-content select="[content-strech-after]"></ng-content>
          </section>
        </div>
  `
})
export class DefaultTemplateComponent {
  constructor() { }
}
