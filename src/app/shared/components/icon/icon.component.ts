import { Component, Input } from '@angular/core';

@Component({
  selector: 'vnd-icon',
  template: `
    <svg [ngClass]="iconClass" viewBox="0 0 24 24" aria-hidden="true">
      <use attr.xlink:href="./assets/images/svg-sprite-icones.svg#{{iconName}}" />
    </svg>
  `,
})
export class IconComponent {

  @Input() iconName: string;
  @Input() iconClass: string;

  constructor() {}

}
