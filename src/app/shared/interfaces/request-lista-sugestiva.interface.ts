export interface IRequestListaSugestiva {
  codigo: number;
  departamento: number;
  ordenacao: string;
  per_page: number;
  page: number;
}
