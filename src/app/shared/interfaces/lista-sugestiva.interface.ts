import { IProduto } from './produto.interface';

export interface IListaSugestiva {
  codigo: number;
  nome: string;
  produtos: Array<IProduto>;
  quantidadeTotalItens: number;
  valorTotalAproximado: number;
}
