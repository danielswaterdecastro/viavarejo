import { IClienteAtendimento } from './cliente-atendimento.interface';
import { IDadosCasamento } from './dados-casamento.interface';
import { Cliente } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';
import { IListaSugestiva } from './lista-sugestiva.interface';

export interface IListaCasamento {
  codigo: number;
  clienteAtendimento: IClienteAtendimento;
  dados: IDadosCasamento;
  listaSugestiva: IListaSugestiva;
  codigoListaCompra: number;
}
