export interface IDadosCasamento {
  cidadeEstado: string;
  data: string;
  emailNoivo: string;
  nomeNoivo: string;
  nomeOutroNoivo: string;
}
