import { RoteadorService, Aplicacao } from 'vv-jornada-core-ngx/dist/integracao';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { CadastroRouteKeys } from '../enums/cadastro-route-keys.enum';
import { CadastroRouteKeysMap } from '../maps/cadastro-route-keys.map';

@Injectable()
export class StepControlService {

  private _step: number;

  constructor(private router: Router,
    private roteadorService: RoteadorService) {
    this._step = 0;
  }

  get step(): number {
    return this._step;
  }

  private updateStep(step: number): void {
    this._step = step;
  }

  private navigate(route: string): void {
    this.router.navigate([`/cadastro/${route}`]);
  }

  public toStep(route: string): void {
    this.updateStep(CadastroRouteKeysMap.get(route));
    this.navigate(route);
  }

  public stepToSuccsess(codigoListaCompra: number ): void {
    this.router.navigate(['/sucesso/'], {queryParams: {codigoListaCompra: codigoListaCompra}});
  }

  public buscarProdutos(): void {
    this.roteadorService.abrir(Aplicacao.CATALOGO);
  }
}
