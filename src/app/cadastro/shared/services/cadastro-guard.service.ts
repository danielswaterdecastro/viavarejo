import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { StepControlService } from './step-control.service';
import { CadastroListaCasamentoService } from './cadastro-lista-casamento.service';
import { CadastroRouteKeys } from '../enums/cadastro-route-keys.enum';
import { CadastroRouteKeysMap } from '../maps/cadastro-route-keys.map';

@Injectable()
export class CadastroGuardService implements CanActivateChild {

  constructor(
    private router: Router,
    private stepControlService: StepControlService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService
  ) { }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const { cadastroListaCasamentoService: cadastro } = this;

    if (cadastro.cliente && cadastro.cliente.codigo) {

      const route = childRoute.routeConfig.path;
      const step = CadastroRouteKeysMap.get(route) || 0;
      return step <= this.stepControlService.step;

    }

    this.goToHome();
    return false;

  }

  private goToHome() {
    this.router.navigate(['/']);
  }

}
