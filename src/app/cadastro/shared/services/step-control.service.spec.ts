import { HttpBuilderModule } from 'vv-jornada-core-ngx/dist/http-builder';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { StepControlService } from './step-control.service';
import { CadastroRouteKeys } from '../enums/cadastro-route-keys.enum';
import { CadastroRouteKeysMap } from '../maps/cadastro-route-keys.map';
import { IntegracaoModule } from 'vv-jornada-core-ngx/dist/integracao';

class RouterStub {
  navigate() { }
}

describe('StepControlService', () => {

  let service: StepControlService;
  let router: Router;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        IntegracaoModule.forRoot(),
        HttpBuilderModule.forRoot()
      ],
      providers: [
        StepControlService,
        { provide: Router, useClass: RouterStub }
      ]
    });

    service = TestBed.get(StepControlService);
    router = TestBed.get(Router);
  });

  it('should return a valid service', () => {
    expect(service).toBeDefined();
  });

  it('should return current step', () => {
    expect(service.step).toBe(0);
  });

  it('should change current step', () => {

    spyOn(router, 'navigate');
    service.toStep(CadastroRouteKeys.CONFIRMACAO);

    expect(service.step).toBe(4);
    expect(router.navigate).toHaveBeenCalled();

  });

});
