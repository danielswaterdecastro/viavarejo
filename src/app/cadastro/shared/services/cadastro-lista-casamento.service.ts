import { IClienteAtendimento } from './../../../shared/interfaces/cliente-atendimento.interface';
import { Injectable } from '@angular/core';
import { IListaCasamento } from './../../../shared/interfaces/lista-casamento.interface';
import { IDadosCasamento } from '../../../shared/interfaces/dados-casamento.interface';
import { IListaSugestiva } from '../../../shared/interfaces/lista-sugestiva.interface';
import { ListaCasamento } from './../../../shared/models/lista-casamento.model';

@Injectable()
export class CadastroListaCasamentoService {

  private _listaCasamento: IListaCasamento;

  constructor() {
    this.resetarListaCasamento();
  }

  public resetarListaCasamento(): void {
    this._listaCasamento = new ListaCasamento();
  }

  get listaCasamento() {
    return this._listaCasamento;
  }

  get cliente() {
    return this._listaCasamento.clienteAtendimento;
  }

  set cliente(cliente: IClienteAtendimento) {
    this._listaCasamento.clienteAtendimento = cliente;
  }

  get dados() {
    return this._listaCasamento.dados;
  }

  set dados(dados: IDadosCasamento) {
    this._listaCasamento.dados = dados;
  }

  get listaSugestiva() {
    return this._listaCasamento.listaSugestiva;
  }

  set listaSugestiva(listaSugestiva: IListaSugestiva) {
    this._listaCasamento.listaSugestiva = listaSugestiva;
  }
}
