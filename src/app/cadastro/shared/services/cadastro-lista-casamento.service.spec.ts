import { IClienteAtendimento } from './../../../shared/interfaces/cliente-atendimento.interface';
import { ClienteAtendimento } from './../../../shared/models/cliente-atendimento.model';
import { IDadosCasamento } from './../../../shared/interfaces/dados-casamento.interface';
import { CadastroListaCasamentoService } from './cadastro-lista-casamento.service';
import { DadosCasamentoMapperService } from '../../scenes/cadastro-dados-casamento/services/dados-casamento-mapper.service';
import { DadosCasamentoSchema } from '../../scenes/cadastro-dados-casamento/schemas/dados-casamento.schema';
import { ListaSugestiva } from '../../../shared';
import { Produto } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';
import { IListaSugestiva } from '../../../shared/interfaces/lista-sugestiva.interface';
import { IProduto } from '../../../shared/interfaces/produto.interface';

describe('CadastroListaCasamentoService ', () => {

  let service: CadastroListaCasamentoService;
  let client: IClienteAtendimento;
  let wedding: IDadosCasamento;
  let sugestionList: IListaSugestiva;

  beforeEach(() => {

    service = new CadastroListaCasamentoService();
    client = new ClienteAtendimento();
    client.codigo = 1;
    DadosCasamentoSchema.setValue({
      'noivoCadastrado': {
        'nome': 'Jhon Doe',
        'email': 'jhon.doe@nobody.com',
        'confirmaEmail': 'jhon.doe@nobody.com'
      },
      'outroNoivo': {
        'nome': 'Jane Doe'
      },
      'evento': {
        'data': '01/01/2049',
        'municipio': 'No Where - NW'
      }
    });
    wedding = new DadosCasamentoMapperService().mapper(DadosCasamentoSchema);
    sugestionList = new ListaSugestiva();
    sugestionList.codigo = 1;
    sugestionList.nome = 'Fogão';
    sugestionList.produtos = new Array<IProduto>();
    sugestionList.quantidadeTotalItens = 0;
    sugestionList.valorTotalAproximado = 0;
  });

  it('should return a valid service', () => {
    expect(service).toBeTruthy();
  });

  it('should set a valid client id', () => {
    service.cliente = client;
    expect(service.cliente.codigo).toBe(1);
  });

  it('should set a valid wedding data', () => {

    service.dados = wedding;

    expect(service.dados.nomeNoivo).toEqual('Jhon Doe');
    expect(service.dados.emailNoivo).toEqual('jhon.doe@nobody.com');
    expect(service.dados.nomeOutroNoivo).toEqual('Jane Doe');
    expect(service.dados.data).toEqual('01/01/2049');
    expect(service.dados.cidadeEstado).toEqual('No Where - NW');

  });

  it('should set a valid sugestion list', () => {
    service.listaSugestiva = sugestionList;
    expect(service.listaSugestiva).toEqual(sugestionList);
  });

  it('should return the completed marriage registration', () => {

    service.cliente = client;
    service.dados = wedding;
    service.listaSugestiva = sugestionList;

    expect(service.listaCasamento.clienteAtendimento).toEqual(client);
    expect(service.listaCasamento.dados).toEqual(wedding);
    expect(service.listaCasamento.listaSugestiva).toEqual(sugestionList);

  });

});
