import { HttpBuilderModule } from 'vv-jornada-core-ngx/dist/http-builder';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TestBed } from '@angular/core/testing';

import { CadastroGuardService } from './cadastro-guard.service';
import { StepControlService } from './step-control.service';
import { CadastroRouteKeys } from '../enums/cadastro-route-keys.enum';
import { CadastroListaCasamentoService } from './cadastro-lista-casamento.service';
import { ClienteAtendimento } from '../../../shared/models/cliente-atendimento.model';
import { RoteadorService, IntegracaoModule } from 'vv-jornada-core-ngx/dist/integracao';

describe('CadastroGuardService', () => {

  let guard: CadastroGuardService;
  let stepControlService: StepControlService;
  let cadastroListaCasamentoService: CadastroListaCasamentoService;
  let clienteAtendimento: ClienteAtendimento;
  let activatedRouteSnapshot: ActivatedRouteSnapshot;

  const router = { navigate: jasmine.createSpy('navigate') };
  const activatedRouteSnapshotMock = { routeConfig: { path: CadastroRouteKeys.DADOS_CASAMENTO } };
  const routerStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IntegracaoModule.forRoot(),
        HttpBuilderModule.forRoot()
      ],
      providers: [
        CadastroListaCasamentoService,
        CadastroGuardService,
        StepControlService,
        { provide: Router, useValue: router },
        {
          provide: ActivatedRouteSnapshot,
          useValue: activatedRouteSnapshotMock
        }
      ]
    });

    guard = TestBed.get(CadastroGuardService);
    stepControlService = TestBed.get(StepControlService);
    cadastroListaCasamentoService = TestBed.get(CadastroListaCasamentoService);
    activatedRouteSnapshot = TestBed.get(ActivatedRouteSnapshot);
    clienteAtendimento = new ClienteAtendimento();
    clienteAtendimento.codigo = 123;

  });

  it('should prevent user to access route', () => {
    expect(guard.canActivateChild(activatedRouteSnapshot, routerStateSnapshot)).toBe(false);
  });

  it('should allow user to access route', () => {
    cadastroListaCasamentoService.cliente = clienteAtendimento;
    stepControlService.toStep(CadastroRouteKeys.DADOS_CASAMENTO);
    expect(guard.canActivateChild(activatedRouteSnapshot, routerStateSnapshot)).toBe(true);
  });

});
