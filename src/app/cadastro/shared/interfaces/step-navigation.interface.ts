export interface IStepNavigation {
  next(): void;
  prev(): void;
}
