import { BreadcrumbService } from './breadcrumb.service';
import { CadastroRouteKeys } from './../../enums/cadastro-route-keys.enum';
import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params } from '@angular/router';
import { IBreadcrumb } from './breadcrumb.interface';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'vnd-breadcrumb',
  template: `
    <div class="breadcrumb">
      <a *ngFor="let breadcrumb of (breadcrumbs.length > 0 ? breadcrumbs.slice(0, -1) : breadcrumbs)"
        [routerLink]="[breadcrumb.url]">{{ breadcrumb.label }}</a>
      <span *ngIf="breadcrumbs.length > 0">
        {{ breadcrumbs[breadcrumbs.length-1].label }}
      </span>
    </div>
    <div class="menu">
      <a [routerLink]="breadcrumbs[breadcrumbs.length-2].url" class="icon is-primary">
        <vnd-icon class="icon-box" [iconName]="'voltar'"></vnd-icon>
      </a>
      <h1 *ngIf="breadcrumbs.length > 0">
        {{ !!breadcrumbs[breadcrumbs.length-1].title? breadcrumbs[breadcrumbs.length-1].title : breadcrumbs[breadcrumbs.length-1].label }}
      </h1>
    </div>
  `,
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  public breadcrumbs;

  constructor(public breadcrumbService: BreadcrumbService) {}

  ngOnInit() {
    this.loadBreadcrumbs();
  }

  loadBreadcrumbs() {
    this.breadcrumbService.breadcrumbs$.subscribe(breadcrumb => {
      this.breadcrumbs = breadcrumb;
    });
  }
}
