import { CadastroRouteKeys } from './../../enums/cadastro-route-keys.enum';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IBreadcrumb } from './breadcrumb.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class BreadcrumbService {
  public breadcrumbsSource = new BehaviorSubject<IBreadcrumb[]>([]);
  public breadcrumbs$ = this.breadcrumbsSource.asObservable();

  constructor() {}

  setBreadcrumb(breadcrumbs: IBreadcrumb[]): void {
    this.breadcrumbsSource.next(breadcrumbs);
  }
}
