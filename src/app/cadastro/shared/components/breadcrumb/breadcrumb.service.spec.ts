import { Subscription } from 'rxjs/Subscription';
import { IBreadcrumb } from './breadcrumb.interface';
import { TestBed, inject } from '@angular/core/testing';

import { BreadcrumbService } from './breadcrumb.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Type } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

describe('BreadcrumbService', () => {
  let service;
  let activeRoute;
  const params: Subject<Params> = new Subject();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BreadcrumbService,
        {
          provide: ActivatedRoute
        }
      ]
    });

    service = TestBed.get(BreadcrumbService);
    activeRoute = TestBed.get(ActivatedRoute);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Não deve possuir rotas', () => {
    const breadcrumbService = TestBed.get(BreadcrumbService);
    const breadcrumbs = breadcrumbService.setBreadcrumb([]);
    const expectedArray: IBreadcrumb[] = [];

    breadcrumbService.breadcrumbs$.subscribe(breadcrumbsResult => {
      expect(breadcrumbsResult).toEqual(expectedArray);
    });
  });

  it('Deve possuir uma rota', () => {
    const breadcrumbService = TestBed.get(BreadcrumbService);
    const breadcrumbs$ = [
      { url: '/', label: 'Painel' },
    ];

    const breadcrumbs = breadcrumbService.setBreadcrumb(breadcrumbs$);
    const expectedArray: IBreadcrumb[] = [
      { url: '/', label: 'Painel' }
    ];

    breadcrumbService.breadcrumbs$.subscribe(breadcrumbsResult => {
      expect(breadcrumbsResult).toEqual(expectedArray);
    });
  });

  it('Deve possuir várias rotas', () => {
    const breadcrumbService = TestBed.get(BreadcrumbService);
    const breadcrumbs$ = [
      { url: '/', label: 'Painel' },
      { url: '/dados-casamento', label: 'Dados Casamento' },
    ];

    const breadcrumbs = breadcrumbService.setBreadcrumb(breadcrumbs$);
    const expectedArray: IBreadcrumb[] = [
      { url: '/', label: 'Painel' },
      { url: '/dados-casamento', label: 'Dados Casamento' }
    ];

    breadcrumbService.breadcrumbs$.subscribe(breadcrumbsResult => {
      expect(breadcrumbsResult).toEqual(expectedArray);
    });
  });
});
