import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbComponent } from './breadcrumb.component';

const breadcrumbRoutes: Routes = [
  { path: 'breadcrumb', component: BreadcrumbComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(breadcrumbRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class BreadcrumbRoutingModule { }
