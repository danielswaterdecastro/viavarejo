import { SharedModule } from './../../../../shared/shared.module';
import { BreadcrumbService } from './breadcrumb.service';
import { BreadcrumbComponent } from './breadcrumb.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
  ],
  exports: [
    BreadcrumbComponent
  ],
  declarations: [
    BreadcrumbComponent,
  ],
  providers: [BreadcrumbService]
})
export class BreadcrumbModule {}
