import { CadastroRouteKeys } from '../enums/cadastro-route-keys.enum';
import { EnumMapper } from '../../../shared/utils/enum-mapper.util';

export const CadastroRouteKeysMap = EnumMapper.toKeys(CadastroRouteKeys);

