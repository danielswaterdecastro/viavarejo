export enum CadastroRouteKeys {
  HOME = 'home',
  DADOS_CASAMENTO = 'dados-casamento',
  SELECAO_LISTA = 'selecao-lista',
  SELECAO_LISTA_DETALHE= 'selecao-lista-detalhe',
  CONFIRMACAO = 'confirmacao',
}
