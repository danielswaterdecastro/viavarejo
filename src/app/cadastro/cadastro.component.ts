import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { skip } from 'rxjs/operators';
import { Aplicacao, RoteadorService } from 'vv-jornada-core-ngx/dist/integracao';
import { Atendimento } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';
import { AtendimentoAtivoService } from 'vv-jornada-core-ngx/dist/solicitar-atendimento/atendimento-ativo.service';
import { CadastroListaCasamentoService } from './shared/services/cadastro-lista-casamento.service';

@Component({
  selector: 'vnd-cadastro',
  template: `
    <vnd-default-template>
      <div header>
        <vnd-breadcrumb></vnd-breadcrumb>
      </div>

    <section container-size="full">
      <router-outlet></router-outlet>
    </section>
  </vnd-default-template>
  `,
})
export class CadastroComponent implements OnInit, OnDestroy {

  atendimentoSubscription: Subscription;

  constructor(private atendimentoAtivoService: AtendimentoAtivoService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private roteadorService: RoteadorService) {
  }

  ngOnInit(): void {
    // deve retornar para a home, quando houver alteração de atendimento
    this.atendimentoSubscription = this.atendimentoAtivoService.pipe(skip(1)).subscribe((atendimento: Atendimento) => {
      const listaCasamento = this.cadastroListaCasamentoService.listaCasamento;
      if (listaCasamento) {
        this.roteadorService.abrir(Aplicacao.LISTA_CASAMENTO);
      }
    });
  }

  ngOnDestroy(): void {
    this.atendimentoSubscription.unsubscribe();
  }
}
