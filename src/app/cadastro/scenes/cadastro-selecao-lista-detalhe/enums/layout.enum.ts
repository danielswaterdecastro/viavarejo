export enum Layout {
  GRID = 'grid-view',
  LIST = 'list-view'
}
