export enum OrderBy {
  RELEVANCIA = 'Relevância',
  MAIS_VENDIDO = 'Mais vendidos',
  ALFABETICA = 'Alfabética'
}
