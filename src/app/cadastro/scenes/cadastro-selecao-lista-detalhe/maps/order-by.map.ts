import { EnumMapper } from '../../../../shared/utils/enum-mapper.util';
import { OrderBy } from '../enums/order-by.enum';

export const OrderByMap = EnumMapper.toValues(OrderBy);

