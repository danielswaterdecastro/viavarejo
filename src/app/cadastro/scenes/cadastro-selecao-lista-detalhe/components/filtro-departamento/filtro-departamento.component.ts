import { Params } from '@angular/router';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { ListaSugestiva } from '../../../../../shared/index';
import { Departamento } from './../../../../../shared/models/departamento.model';
import { ListaCasamentoRestService } from './../../../../../shared/services/lista-casamento-rest.service';
import { IListaSugestiva } from '../../../../../shared/interfaces/lista-sugestiva.interface';

@Component({
  selector: 'vnd-filtro-departamento-lista',
  template: `
  <section class="accordion">
    <header [ngClass]="{ 'is-open': filtroAberto }" (click)="filtroAberto = !filtroAberto">
      <button class="header-title" type="button">Departamento</button>
    </header>
    <div class="content">
      <ul>
        <li *ngFor="let departamento of listaCasamentoRestService.departamento$ | async">
          <a href="javascript:void(0)" (click)="setDepartment(departamento.codigo)">
            {{ departamento.nome }} <span>({{ departamento.quantidadeProdutos }})</span>
          </a>
        </li>
        <li class="last">
          <a href="javascript:void(0)" (click)="setDepartment()" class="is-button btn-all">
            Todos <span>({{ productsAmount }})</span>
          </a>
        </li>
      </ul>
    </div>
  </section>
  `,
  styleUrls: ['./filtro-departamento.scss'],
})
export class FiltroDepartamentoComponent implements OnInit, OnDestroy {

  public filtroAberto = true;
  public id: number;
  public productsAmount = 0;
  private suggestiveListSubscription = new Subscription();
  private subscriptions = new Subscription();


  @Output()
  public department = new EventEmitter<number>();

  constructor(
    public listaCasamentoRestService: ListaCasamentoRestService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.applyParams();
    this.applyProductsAmount();
  }

  private applyParams(): void {
    const subscription = this.route.params.subscribe((params: Params) => {
      this.id = params.id;
      this.listaCasamentoRestService.buscarDepartamentos(this.id);
    });

    this.subscriptions.add(subscription);
  }

  private applyProductsAmount(): void {
    this.suggestiveListSubscription = this.listaCasamentoRestService.listaSugestiva$
      .subscribe((list: IListaSugestiva) => {

        if (list.hasOwnProperty('quantidadeTotalItens')) {
          this.productsAmount = list.quantidadeTotalItens;
          this.suggestiveListSubscription.unsubscribe();
        }

    });

  }

  public setDepartment(department?: number): void {
    this.department.emit(department);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}

