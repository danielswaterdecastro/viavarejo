import { BreadcrumbService } from './../../shared/components/breadcrumb/breadcrumb.service';
import { IListaSugestiva } from './../../../shared/interfaces/lista-sugestiva.interface';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { timer } from 'rxjs/observable/timer';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Observable } from 'rxjs/Observable';

import { ModalService } from 'vv-jornada-core-ngx/dist/modal';
import { IProduto } from '../../../shared/interfaces/produto.interface';
import { IStepNavigation } from '../../shared/interfaces/step-navigation.interface';
import { StepControlService } from '../../shared/services/step-control.service';
import { ListaCasamentoRestService } from '../../../shared/services/lista-casamento-rest.service';
import { OrderBy } from './enums/order-by.enum';
import { Layout } from './enums/layout.enum';
import { CadastroRouteKeys } from '../../shared/enums/cadastro-route-keys.enum';
import { OrderByMap } from './maps/order-by.map';
import { EnumTransform } from '../../../shared/utils/enum-transform.util';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PaginationService } from '../../../shared/services/pagination.service';
import { CadastroListaCasamentoService } from '../../shared/services/cadastro-lista-casamento.service';
import { ListaSugestiva } from '../../../shared';

@Component({
  selector: 'vnd-cadastro-selecao-lista-detalhe',
  templateUrl: './cadastro-selecao-lista-detalhe.template.html',
  styleUrls: ['./selecao-lista-detalhe.component.scss'],
  providers: [PaginationService]
})
export class CadastroSelecaoListaDetalheComponent implements OnInit, OnDestroy, IStepNavigation {

  public id: number;
  public order: string;
  public department: number;
  public filters: Array<string>;
  private layout: Layout;
  public isConfirmDisabled: boolean;
  public suggestionListHolder: IListaSugestiva;

  private subscriptions = new Subscription();
  public layoutTypes = Layout;

  private breadcrumbs$ = [
    { url: `/${CadastroRouteKeys.HOME}`, label: 'Painel', title: 'Painel' },
    { url: CadastroRouteKeys.DADOS_CASAMENTO, label: 'Dados do Casamento', title: 'Dados do Casamento' },
    { url: CadastroRouteKeys.SELECAO_LISTA, label: 'Seleção da Lista', title: 'Seleção da Lista' },
    { url: CadastroRouteKeys.SELECAO_LISTA_DETALHE, label: 'Detalhes da Lista', title: 'Detalhes da Lista' }
  ];

  constructor(
    private route: ActivatedRoute,
    private stepControlService: StepControlService,
    private router: Router,
    public listaCasamentoRestService: ListaCasamentoRestService,
    public paginationService: PaginationService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private breadcrumbService: BreadcrumbService,
    private modalService: ModalService
  ) {
    this.order = OrderByMap.get(OrderBy.RELEVANCIA);
    this.filters = EnumTransform.toArray(OrderBy);
    this.layout = Layout.GRID;
    this.isConfirmDisabled = true;
  }

  ngOnInit(): void {
    this.applyPaginationSettings();
    this.applyParams();
    this.applyListaSugerida();
    this.applyBreadcrumb();
  }

  private applyBreadcrumb(): void {
    this.listaCasamentoRestService.listaSugestiva$.subscribe(listaSugestivaAtual => {
      this.updateLastItemBreadcrumb(`Lista ${listaSugestivaAtual.nome}`);
    });
    this.updateLastItemBreadcrumb(this.breadcrumbs$[this.breadcrumbs$.length - 1].label);
    this.breadcrumbService.setBreadcrumb(this.breadcrumbs$);
  }

  private updateLastItemBreadcrumb(title: string): void {
    this.breadcrumbs$[this.breadcrumbs$.length - 1].title = title;
  }

  private applyPaginationSettings(): void {
    this.page = 1;
    this.max = 1;
    this.offset = 9;
  }

  private applyParams(): void {

    const subscription = this.observeParams().subscribe(props => {

      const { params, queryParams } = props;

      this.resolveParams(params);
      this.resolveQueryParams(queryParams);
      this.requestProducList();

    }, erro => this.modalService.criarAlertaErro(erro));

    this.subscriptions.add(subscription);
  }

  private observeParams(): Observable<{ params: Params; queryParams: Params; }> {
    return combineLatest(
      this.route.params,
      this.route.queryParams,
      (params, queryParams) => ({ params, queryParams })
    );
  }

  private resolveParams(params: Params): void {
    if (params.hasOwnProperty('id')) {
      this.id = Number(params['id']);
    }
  }

  private resolveQueryParams(queryParams: Params): void {

    if (queryParams.hasOwnProperty('codigo')) {
      this.department = Number(queryParams['codigo']);
    }

    if (queryParams.hasOwnProperty('ordenacao')) {
      this.order = String(queryParams['ordenacao']);
    }

  }

  public applyDepartment(department: number): void {
    this.department = department;
    this.applyPaginationSettings();
    this.updateQueryParams({ ...this.route.snapshot.queryParams, departamento: this.department });
  }

  private applyListaSugerida(): void {

    const subscription = this.listaCasamentoRestService.listaSugestiva$.subscribe((lista: IListaSugestiva) => {
      this.applyMax(lista);
      this.saveListaSugestiva(lista);
    }, erro => this.modalService.criarAlertaErro(erro));

    this.subscriptions.add(subscription);

  }

  private saveListaSugestiva(list: IListaSugestiva): void {
    if (Object.keys(list).length !== 0) {
      this.suggestionListHolder = list;
      this.isConfirmDisabled = false;
    }
  }

  private storeSuggestionList(): void {
    this.cadastroListaCasamentoService.listaSugestiva = this.suggestionListHolder;
  }

  private applyMax(lista: IListaSugestiva): void {
    this.paginationService.calcMaxPage(lista.quantidadeTotalItens);
  }

  private requestProducList(): void {
    const { id, department, order, offset, page } = this;
    this.listaCasamentoRestService.buscarDetalhesListaSugestiva(id, department, order, offset, page);
  }

  private resolveOrder(filter: OrderBy) {
    this.order = OrderByMap.get(filter);
  }

  public orderBy(order: OrderBy): void {
    this.resolveOrder(order);
    this.updateQueryParams({ ...this.route.snapshot.queryParams, ordenacao: this.order });
  }

  private updateQueryParams(queryParams: Object): void {
    this.router.navigate(['.'], { relativeTo: this.route, queryParams });
  }

  public paginationBy(page: number): void {
    if (page > 0 && page <= this.max) {
      this.page = page;
      this.requestProducList();
    }
  }

  public getLayout(): string {
    return this.layout;
  }

  public setLayout(layout: Layout): void {
    this.layout = layout;
  }

  public isLayout(layout: Layout): boolean {
    return this.getLayout() === layout;
  }

  public next(): void {
    this.storeSuggestionList();
    this.stepControlService.toStep(CadastroRouteKeys.CONFIRMACAO);
  }

  public prev(): void {
    this.stepControlService.toStep(CadastroRouteKeys.SELECAO_LISTA);
  }

  public paginationNext(page: number): void {
    this.paginationService.next();
    this.requestProducList();
  }

  public paginationPrev(page: number): void {
    this.paginationService.prev();
    this.requestProducList();
  }

  get page() {
    return this.paginationService.page;
  }

  set page(page: number) {
    this.paginationService.page = page;
  }

  get max() {
    return this.paginationService.max;
  }

  set max(max: number) {
    this.paginationService.max = max;
  }

  get offset() {
    return this.paginationService.offset;
  }

  set offset(offset: number) {
    this.paginationService.offset = offset;
  }

  ngOnDestroy() {
    this.listaCasamentoRestService.clearProducts();
    this.listaCasamentoRestService.clearDepartment();
    this.subscriptions.unsubscribe();
  }

}

