import { FormGroup } from '@angular/forms';

export function areEmailEquals(group: FormGroup) {

  const email = group.get('noivoCadastrado').get('email').value;
  const confirmaEmail = group.get('noivoCadastrado').get('confirmaEmail').value;

  return email === confirmaEmail ? null : { 'areEmailEquals': true };

}
