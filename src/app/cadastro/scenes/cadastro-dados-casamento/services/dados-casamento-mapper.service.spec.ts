import { FormGroup } from '@angular/forms';
import { DadosCasamentoMapperService } from './dados-casamento-mapper.service';
import { DadosCasamentoSchema } from '../schemas/dados-casamento.schema';
import { DadosCasamento } from '../../../../shared/models/dados-casamento.model';

describe('DadosCasamentoMapperService', () => {

  let service: DadosCasamentoMapperService;
  let schema: FormGroup;
  let dadosCasamento: DadosCasamento;

  beforeEach(() => {

    service = new DadosCasamentoMapperService();
    schema = DadosCasamentoSchema;
    schema.setValue({
      'noivoCadastrado': {
        'nome': 'Jhon Doe',
        'email': 'jhon.doe@nobody.com',
        'confirmaEmail': 'jhon.doe@nobody.com'
      },
      'outroNoivo': {
        'nome': 'Jane Doe'
      },
      'evento': {
        'data': '01/01/2049',
        'municipio': 'No Where - NW'
      }
    });

  });


  it('deve mapear dados do formulário para a classe DadosCasamento com sucesso', () => {

    dadosCasamento = service.mapper(schema);

    expect(dadosCasamento.nomeNoivo).toEqual('Jhon Doe');
    expect(dadosCasamento.emailNoivo).toEqual('jhon.doe@nobody.com');
    expect(dadosCasamento.nomeOutroNoivo).toEqual('Jane Doe');
    expect(dadosCasamento.data).toEqual('01/01/2049');
    expect(dadosCasamento.cidadeEstado).toEqual('No Where - NW');

  });

});
