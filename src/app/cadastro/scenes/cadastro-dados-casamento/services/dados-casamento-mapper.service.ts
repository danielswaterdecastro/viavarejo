import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { DadosCasamento } from './../../../../shared/models/dados-casamento.model';
import { IDadosCasamento } from './../../../../shared/interfaces/dados-casamento.interface';

@Injectable()
export class DadosCasamentoMapperService {

  public mapper(group: FormGroup): IDadosCasamento {

    const dadosCasamento = new DadosCasamento();

    dadosCasamento.nomeNoivo = group.get('noivoCadastrado').get('nome').value;
    dadosCasamento.emailNoivo = group.get('noivoCadastrado').get('email').value;
    dadosCasamento.nomeOutroNoivo = group.get('outroNoivo').get('nome').value;
    dadosCasamento.data = group.get('evento').get('data').value;
    dadosCasamento.cidadeEstado = group.get('evento').get('municipio').value;

    return dadosCasamento;

  }

}
