import { BreadcrumbService } from './../../shared/components/breadcrumb/breadcrumb.service';
import { CadastroListaCasamentoService } from './../../shared/services/cadastro-lista-casamento.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageIntegracaoService } from 'vv-jornada-core-ngx/dist/integracao';
import { DadosCasamentoMapperService } from './services/dados-casamento-mapper.service';
import { CadastroRouteKeys } from '../../shared/enums/cadastro-route-keys.enum';
import { DadosCasamentoSchema } from './schemas/dados-casamento.schema';
import { Cliente } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';
import { StepControlService } from '../../shared/services/step-control.service';
import { IStepNavigation } from '../../shared/interfaces/step-navigation.interface';
import * as moment from 'moment';


@Component({
  selector: 'vnd-cadastro-noivos',
  templateUrl: './cadastro-dados-casamento.template.html',
  styleUrls: ['./cadastro-dados-casamento.style.scss']
})
export class CadastroDadosCasamentoComponent implements OnInit, IStepNavigation {

  form: FormGroup;
  nome: AbstractControl;
  email: AbstractControl;
  confirmaEmail: AbstractControl;
  nomeOutroNoivo: AbstractControl;
  data: AbstractControl;
  private _municipio: AbstractControl;

  private breadcrumbs$ = [
    { url: `/${CadastroRouteKeys.HOME}`, label: 'Painel' },
    { url: CadastroRouteKeys.DADOS_CASAMENTO, label: 'Dados do Casamento' }
  ];

  constructor(
    private storageIntegracaoService: StorageIntegracaoService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private dadosCasamentoMapperService: DadosCasamentoMapperService,
    private stepControlService: StepControlService,
    private router: Router,
    private breadcrumbService: BreadcrumbService
  ) {

    DadosCasamentoSchema.reset();
    this.form = DadosCasamentoSchema;

    this.nome = this.getFormItem('noivoCadastrado', 'nome');
    this.email = this.getFormItem('noivoCadastrado', 'email');
    this.confirmaEmail = this.getFormItem('noivoCadastrado', 'confirmaEmail');
    this.nomeOutroNoivo = this.getFormItem('outroNoivo', 'nome');
    this.data = this.getFormItem('evento', 'data');
    this._municipio = this.getFormItem('evento', 'municipio');
  }

  public ngOnInit(): void {
    this.applyClientData();

    this.breadcrumbService.setBreadcrumb(this.breadcrumbs$);
  }

  private applyClientData() {
    this.nome.setValue(this.cadastroListaCasamentoService.dados.nomeNoivo);
    this.email.setValue(this.cadastroListaCasamentoService.dados.emailNoivo);
    this.confirmaEmail.setValue(this.cadastroListaCasamentoService.dados.emailNoivo);
    this.nomeOutroNoivo.setValue(this.cadastroListaCasamentoService.dados.nomeOutroNoivo);
    this.data.setValue(this.cadastroListaCasamentoService.dados.data);
    this._municipio.setValue(this.cadastroListaCasamentoService.dados.cidadeEstado);
  }

  get municipio() {
    return this._municipio.value;
  }

  set municipio(value: string) {
    this._municipio.setValue(value);
    this._municipio.markAsDirty();
    this._municipio.markAsTouched();
  }

  private getFormItem(group: string, name: string): AbstractControl {
    return this.form.get(`${group}.${name}`);
  }

  public hasError(prop: string): boolean {
    return this[prop].invalid && (this[prop].dirty || this[prop].touched);
  }

  public areEmailEquals(): boolean {
    if (this.email.valid && this.confirmaEmail.valid && this.form.errors) {
      return this.form.errors.areEmailEquals;
    }
  }

  public dateValidateMaximum(): boolean {
    const currentDate = moment();
    const diff = moment(this.data.value, 'DD/MM/YYYY')
      .diff(moment(currentDate, 'DD/MM/YYYY'), 'days', true);
    if (diff > 365) {
      this.data.markAsPending();
    }
    return diff > 365;
  }

  public dateValidateMinimum(): boolean {
    const currentDate = moment();
    if (moment(this.data.value, 'DD/MM/YYYY')
          .isBefore(moment(currentDate, 'DD/MM/YYYY'))) {
      this.data.markAsPending();
    }
    return moment(this.data.value, 'DD/MM/YYYY').isBefore(moment(currentDate, 'DD/MM/YYYY'));
  }

  public next(): void {
    this.stepControlService.toStep(CadastroRouteKeys.SELECAO_LISTA);
  }

  public prev(): void {
    this.router.navigate(['/']);
  }

  public submit(group: FormGroup): void {
    this.cadastroListaCasamentoService.dados = this.dadosCasamentoMapperService.mapper(group);
    this.next();
  }

  public possuiCadastroSite(): boolean {
    return this.cadastroListaCasamentoService.cliente.possuiCadastroSite;
  }

}
