import { FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { areEmailEquals } from './../validators/are-email-equals.validator';

const fb = new FormBuilder();

export const DadosCasamentoSchema = fb.group({
  'noivoCadastrado': fb.group({
    'nome': [ null, Validators.required ],
    'email': [ null, [ Validators.required, Validators.email ] ],
    'confirmaEmail': [ null, [ Validators.required, Validators.email ] ]
  }),
  'outroNoivo': fb.group({
    'nome': [ null, Validators.required ]
  }),
  'evento': fb.group({
    'data': [ null, Validators.required ],
    'municipio': [ null, Validators.required ]
  })
}, { validator: areEmailEquals });
