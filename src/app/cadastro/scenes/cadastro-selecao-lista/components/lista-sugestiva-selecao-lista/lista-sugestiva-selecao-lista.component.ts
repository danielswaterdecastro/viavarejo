import { Produto } from 'vv-jornada-core-ngx/dist/solicitar-atendimento/model/produto.model';
import { ListaSugestiva } from './../../../../../shared/models/lista-sugestiva.model';
import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, LOCALE_ID, OnInit } from '@angular/core';

@Component({
  selector: 'vnd-lista-sugestiva-selecao-lista',
  template: `
    <section class="lista-sugerida" *ngFor="let lista of listas">
      <header>
        <h3 class="header-title">{{ lista.nome }}</h3>
        <div class="header-content" element-wrapper container-size="xxlarge">
          <span element-size="large">Valor aproximado de produtos desta lista</span>
          <strong element-size="xsmall">{{ lista.valorTotalAproximado | preco }}</strong>
          <span element-size="xxsmall">({{ lista.quantidadeTotalItens }} itens)</span>
        </div>
      </header>
      <section>
        <ul>
          <li *ngFor="let produto of lista.produtos" class="produto-sugerido">
            <img [src]="produto.urlImagem" [alt]="produto.nome">
            <strong>{{ produto.nome }}</strong>
          </li>
        </ul>
      </section>
      <div class="actions">
      <a [routerLink]="['/cadastro/selecao-lista-detalhe', lista.codigo]" class="is-secondary">Ver Detalhes</a>
        <span class="radio">
          <input
            value="{{ lista.codigo }}"
            (change)="selecionarLista(lista)"
            [checked]="lista.codigo === listaAtualSelecionada.codigo"
            name="lista-selecionada"
            id="'lista-selecionada-'{{ lista.codigo }}"
            type="checkbox">
          <label for="'lista-selecionada-'{{ lista.codigo }}">Selecionar</label>
        </span>
      </div>
    </section>
    <p *ngIf="mensagemErro" class="message-box is-warning" role="alert" [innerHTML]="mensagemErro"></p>
  `,
  styleUrls: ['lista-sugestiva-selecao-lista.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }]
})
export class ListaSugestivaSelecaoListaComponent implements OnInit {

  @Input() listaPreviamenteSelecionada: ListaSugestiva;
  @Input() listas: Array<ListaSugestiva> = new Array<ListaSugestiva>();
  private listaAtualSelecionada: ListaSugestiva = new ListaSugestiva();

  @Input() mensagemErro: string;

  @Output()
  selecionar = new EventEmitter<ListaSugestiva>();

  constructor() { }

  ngOnInit(): void {
    this.listaAtualSelecionada = this.listaPreviamenteSelecionada;
    this.selecionar.emit(this.listaAtualSelecionada);
  }

  selecionarLista(lista: ListaSugestiva) {
    let itemSelecionado = this.listas.find(itemAtual => itemAtual.codigo === lista.codigo);

    if (this.listaAtualSelecionada.codigo === itemSelecionado.codigo) {
      itemSelecionado = new ListaSugestiva();
    }

    this.listaAtualSelecionada = itemSelecionado;
    this.selecionar.emit(this.listaAtualSelecionada);
  }

}
