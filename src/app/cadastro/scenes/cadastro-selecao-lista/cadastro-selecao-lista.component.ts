import { BreadcrumbService } from './../../shared/components/breadcrumb/breadcrumb.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Aplicacao } from 'vv-jornada-core-ngx/dist/integracao/contratos/aplicacao.enum';
import { RoteadorService } from 'vv-jornada-core-ngx/dist/integracao';
import { ListaCasamentoRestService } from './../../../shared/services/lista-casamento-rest.service';
import { CadastroListaCasamentoService } from '../../shared/services/cadastro-lista-casamento.service';
import { StepControlService } from '../../shared/services/step-control.service';
import { CadastroSelecaoListaService } from './services/cadastro-selecao-lista.service';
import { ListaSugestiva } from './../../../shared/models/lista-sugestiva.model';
import { CadastroRouteKeys } from '../../shared/enums/cadastro-route-keys.enum';
import { IStepNavigation } from '../../shared/interfaces/step-navigation.interface';
import { ModalService } from 'vv-jornada-core-ngx/dist/modal';

@Component({
  selector: 'vnd-cadastro-selecao-lista',
  template: `
  <div container-wrapper>
    <div class="container" container-size="full">
      <vnd-lista-sugestiva-selecao-lista
        [listas]="listasFiltradas"
        [listaPreviamenteSelecionada]="listaPreviamenteSelecionada"
        (selecionar)="marcarListaSugestiva($event)"
        [mensagemErro]="cadastroListaSugestivaService.errorHandler$ | async"
        >
      </vnd-lista-sugestiva-selecao-lista>
    </div>
    <footer>
      <button type="button" class="secondary-action" (click)="prev()">Voltar</button>
      <button type="button" class="main-action" (click)="submit()">Continuar</button>
    </footer>
</div>
  `,
  styleUrls: ['./selecao-lista.component.scss'],
  providers: [CadastroSelecaoListaService]
})
export class CadastroSelecaoListaComponent implements OnInit, OnDestroy, IStepNavigation {

  private listaSelecionada: ListaSugestiva = new ListaSugestiva();
  private listasSubscription: Subscription;

  public buscaVazia: boolean;
  public listasFiltradas: ListaSugestiva[];
  public listaPreviamenteSelecionada: ListaSugestiva = new ListaSugestiva();

  private breadcrumbs$ = [
    { url: `/${CadastroRouteKeys.HOME}`, label: 'Painel' },
    { url: CadastroRouteKeys.DADOS_CASAMENTO, label: 'Dados do Casamento' },
    { url: CadastroRouteKeys.SELECAO_LISTA, label: 'Seleção da Lista' }
  ];

  constructor(
    private listaCasamentoRestService: ListaCasamentoRestService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private stepControlService: StepControlService,
    public cadastroListaSugestivaService: CadastroSelecaoListaService,
    private route: ActivatedRoute,
    private router: Router,
    private roteadorService: RoteadorService,
    private breadcrumbService: BreadcrumbService,
    private modalService: ModalService
  ) { }

  ngOnInit(): void {

    this.listaCasamentoRestService.clearSuggestiveList();
    this.cadastroListaSugestivaService.buscarListasSugestivas();

    this.listasSubscription = this.cadastroListaSugestivaService.listaSugestiva$
      .subscribe((listas: ListaSugestiva[]) => {
        this.buscaVazia = (listas && listas.length === 0);
        if (!this.buscaVazia) {
          this.listasFiltradas = listas.length === 0 && this.listasFiltradas.length !== 0 ? this.listasFiltradas : listas;
        }
      }, erro => this.modalService.criarAlertaErro(erro));

    this.applySelectedListData();

    this.breadcrumbService.setBreadcrumb(this.breadcrumbs$);
  }

  ngOnDestroy(): void {
    this.listasSubscription.unsubscribe();
  }

  private applySelectedListData() {
    this.listaPreviamenteSelecionada = !!this.cadastroListaCasamentoService.listaSugestiva ?
      this.cadastroListaCasamentoService.listaSugestiva : this.listaPreviamenteSelecionada;
  }

  public marcarListaSugestiva(listaSelecionada: ListaSugestiva): void {
    this.listaSelecionada = listaSelecionada;
  }

  public prev(): void {
    this.stepControlService.toStep(CadastroRouteKeys.DADOS_CASAMENTO);
  }

  public next(): void {
    this.stepControlService.toStep(CadastroRouteKeys.CONFIRMACAO);
  }

  public submit(): void {
    this.cadastroListaCasamentoService.listaSugestiva = this.listaSelecionada;
    this.next();
  }

}
