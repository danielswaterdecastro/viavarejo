import { Subscription } from 'rxjs/Subscription';
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ListaSugestiva } from './../../../../shared/models/lista-sugestiva.model';
import { ListaCasamentoRestService } from './../../../../shared/services/lista-casamento-rest.service';

@Injectable()
export class CadastroSelecaoListaService implements OnDestroy {

  private listaSugestivaSource = new BehaviorSubject<ListaSugestiva[]>([]);
  public listaSugestiva$ = this.listaSugestivaSource.asObservable();

  private errorHandlerSource = new ReplaySubject<string>();
  public errorHandler$ = this.errorHandlerSource.asObservable();

  public buscaSubscription: Subscription;

  private readonly message = 'Nenhuma lista pronta foi encontrada.<br>Você pode criar uma Lista de Casamento sem produtos adicionados.';

  constructor(private listaCasamentoRestService: ListaCasamentoRestService) { }

  ngOnDestroy(): void {
    if (this.buscaSubscription) {
      this.buscaSubscription.unsubscribe();
    }
  }

  public buscarListasSugestivas(): void {
    this.buscaSubscription = this.listaCasamentoRestService.buscarListasSugestivas()
      .subscribe((listasSugestivas: Array<ListaSugestiva>) => {

        if (listasSugestivas.length === 0) {
          this.errorHandlerSource.next(this.message);

        } else {

          this.listaSugestivaSource.next(listasSugestivas);
        }
      }, (error) => {
        this.errorHandlerSource.next(this.message);

        return { unsubscribe() { } };
      }, () => {
        return { unsubscribe() { } };
      });
  }
}
