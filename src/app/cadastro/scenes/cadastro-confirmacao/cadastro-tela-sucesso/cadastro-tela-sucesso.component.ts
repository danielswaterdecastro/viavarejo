import { Component, OnInit } from '@angular/core';
import { ListaCasamento } from '../../../../shared';
import { StepControlService } from '../../../shared/services/step-control.service';
import { StorageIntegracaoService, StorageHomeVendedor, Aplicacao } from 'vv-jornada-core-ngx/dist/integracao';
import { CadastroListaCasamentoService } from '../../../shared/services/cadastro-lista-casamento.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ParamMap } from '@angular/router/src/shared';

@Component({
  selector: 'vnd-cadastro-tela-sucesso',
  template: `
  <vnd-default-template>
    <div header>
      <h1>Lista criada com sucesso</h1>
    </div>
    <section container-wrapper>
       <div class="wrapper-logo" container-size="xsmall">
        <img src="./assets/images/logo-listacasamento.png">
       </div>
        <div container-size="xxlarge" class="info-lista">
        {{ vendedor?.nome }}, não se esqueça de avisar o cliente que um e-mail será enviado para ativação do cadastro.
            <p>Avise, também, que o gerenciamento da Lista deve ser feito pelo site.</p>
            <small class="list-code">Código da Lista: {{ codigoListaCompra }}</small>
            <button class="main-action" type="button" (click)="stepControlService.buscarProdutos()">
              Continuar Atendimento
            </button>
        </div>
    </section>
  </vnd-default-template>
  `,
  styleUrls: ['./cadastro-tela-sucesso.component.scss']
})
export class CadastroTelaSucessoComponent implements OnInit {
  vendedor: StorageHomeVendedor;
  codigoListaCompra: number;

  constructor(public stepControlService: StepControlService,
    private storage: StorageIntegracaoService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private route:  ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((parametro: Params) => {
      this.codigoListaCompra = parametro.codigoListaCompra;
    });
    this.storage.lerLogin().then(login => {
      this.vendedor = login.vendedor;
    });
  }
}
