import { Component, LOCALE_ID, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { StorageHomeVendedor, StorageIntegracaoService } from 'vv-jornada-core-ngx/dist/integracao';
import { ModalService } from 'vv-jornada-core-ngx/dist/modal';
import { ListaCasamento } from '../../../shared/index';
import { CadastroRouteKeys } from '../../shared/enums/cadastro-route-keys.enum';
import { IStepNavigation } from '../../shared/interfaces/step-navigation.interface';
import { StepControlService } from '../../shared/services/step-control.service';
import { ListaCasamentoRestService } from './../../../shared/services/lista-casamento-rest.service';
import { BreadcrumbService } from './../../shared/components/breadcrumb/breadcrumb.service';
import { CadastroListaCasamentoService } from './../../shared/services/cadastro-lista-casamento.service';




@Component({
  selector: 'vnd-cadastro-confirmacao',
  templateUrl: './cadastro-confirmacao.component.html',
  styleUrls: ['./cadastro-confirmacao.component.scss'],
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }]
})
export class CadastroConfirmacaoComponent implements IStepNavigation, OnInit, OnDestroy {

  vendedor: StorageHomeVendedor;
  listaCasamento: ListaCasamento;
  possuiLista: boolean;

  private breadcrumbs$ = [
    { url: `/${CadastroRouteKeys.HOME}`, label: 'Painel' },
    { url: CadastroRouteKeys.DADOS_CASAMENTO, label: 'Dados do Casamento' },
    { url: CadastroRouteKeys.SELECAO_LISTA, label: 'Seleção da Lista' },
    { url: CadastroRouteKeys.CONFIRMACAO, label: 'Confirmação', title: 'Confirmar Dados' }
  ];

  public cadastrarSubscription: Subscription;

  constructor(private stepControlService: StepControlService,
    private cadastroListaCasamentoService: CadastroListaCasamentoService,
    private listaCasamentoRestService: ListaCasamentoRestService,
    private modalService: ModalService,
    private storage: StorageIntegracaoService,
    private breadcrumbService: BreadcrumbService) { }

  ngOnInit(): void {
    this.listaCasamento = this.cadastroListaCasamentoService.listaCasamento;
    this.possuiLista = this.listaCasamento.listaSugestiva.codigo > 0;

    this.breadcrumbService.setBreadcrumb(this.breadcrumbs$);

    this.storage.lerLogin().then(login => {
      this.vendedor = login.vendedor;
    });
  }

  ngOnDestroy(): void {
    if (this.cadastrarSubscription) {
      this.cadastrarSubscription.unsubscribe();
    }
  }

  next(): void {

    this.cadastrarSubscription = this.listaCasamentoRestService.cadastrarListaCasamento(this.listaCasamento).subscribe(
      response => {
        this.listaCasamento.codigoListaCompra = response.codigoListaCompra;
        this.stepControlService.stepToSuccsess(this.listaCasamento.codigoListaCompra);
        this.cadastroListaCasamentoService.resetarListaCasamento();
      },
      error => {
        this.modalService.criarAlertaErro(error);
      });
  }

  prev(): void {
    this.stepControlService.toStep(CadastroRouteKeys.SELECAO_LISTA);
  }
}
