import { CadastroComponent } from './cadastro.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { routes as cadastroRotas } from './cadastro.routes';

const routes: Routes = [
  { path: '', component: CadastroComponent,
    children: [...cadastroRotas]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadastroRoutingModule { }
