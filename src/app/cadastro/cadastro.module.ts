import { FormularioBasicoModule } from 'vv-jornada-core-ngx/dist/formulario-basico';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BreadcrumbModule } from './shared/components/breadcrumb/breadcrumb.module';

import { CadastroRoutingModule } from './cadastro-routing.module';
import { CadastroSelecaoListaDetalheComponent } from './scenes/cadastro-selecao-lista-detalhe/cadastro-selecao-lista-detalhe.component';
import { SharedModule } from '../shared/shared.module';
import { ListaCasamentoRestService } from './../shared/services/lista-casamento-rest.service';
import { DadosCasamentoMapperService } from './scenes/cadastro-dados-casamento/services/dados-casamento-mapper.service';

import { CadastroComponent } from './cadastro.component';
import { CadastroSelecaoListaComponent } from './scenes/cadastro-selecao-lista/cadastro-selecao-lista.component';
import { CadastroConfirmacaoComponent } from './scenes/cadastro-confirmacao/cadastro-confirmacao.component';
import { CadastroDadosCasamentoComponent } from './scenes/cadastro-dados-casamento/cadastro-dados-casamento.component';
import {
  FiltroDepartamentoComponent
} from './scenes/cadastro-selecao-lista-detalhe/components/filtro-departamento/filtro-departamento.component';
import {
  ListaSugestivaSelecaoListaComponent
} from './scenes/cadastro-selecao-lista/components/lista-sugestiva-selecao-lista/lista-sugestiva-selecao-lista.component';

import { PrecoModule } from 'vv-jornada-core-ngx/dist/preco/preco.module';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CadastroRoutingModule,
    FormularioBasicoModule,
    BreadcrumbModule,
    PrecoModule
  ],
  declarations: [
    CadastroComponent,
    CadastroConfirmacaoComponent,
    CadastroDadosCasamentoComponent,
    CadastroSelecaoListaComponent,
    ListaSugestivaSelecaoListaComponent,
    CadastroSelecaoListaDetalheComponent,
    FiltroDepartamentoComponent
  ],
  providers: [
    ListaCasamentoRestService,
    DadosCasamentoMapperService,
  ]
})
export class CadastroModule { }
