import { Routes } from '@angular/router';
import { CadastroRouteKeys } from './shared/enums/cadastro-route-keys.enum';

import { CadastroDadosCasamentoComponent } from './scenes/cadastro-dados-casamento/cadastro-dados-casamento.component';
import { CadastroComponent } from './cadastro.component';
import { CadastroConfirmacaoComponent } from './scenes/cadastro-confirmacao/cadastro-confirmacao.component';
import { CadastroSelecaoListaComponent } from './scenes/cadastro-selecao-lista/cadastro-selecao-lista.component';
import { CadastroSelecaoListaDetalheComponent } from './scenes/cadastro-selecao-lista-detalhe/cadastro-selecao-lista-detalhe.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: CadastroRouteKeys.DADOS_CASAMENTO,
    pathMatch: 'full'
  },
  {
    path: CadastroRouteKeys.DADOS_CASAMENTO,
    component: CadastroDadosCasamentoComponent,
  },
  {
    path: CadastroRouteKeys.SELECAO_LISTA,
    component: CadastroSelecaoListaComponent,
  },
  {
    path: `${CadastroRouteKeys.SELECAO_LISTA_DETALHE}/:id`,
    component: CadastroSelecaoListaDetalheComponent,
  },
  {
    path: CadastroRouteKeys.CONFIRMACAO,
    component: CadastroConfirmacaoComponent,
  }
];
