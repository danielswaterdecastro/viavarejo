export const Endpoints = {
    listaCasamento: {
        buscarListasSugestivas: () => `vv-lista-casamento/listas-sugestivas`,
        detalhesListaSugestiva: (codigo) => `vv-lista-casamento/listas-sugestivas/${codigo}`,
        clienteSite: (cpf) => `vv-lista-casamento/clientes/${cpf}`,
        departamentosListaSugestiva: (codigo) => `vv-lista-casamento/listas-sugestivas/${codigo}/departamentos`,
        cadastrarListaCasamento: () => `vv-lista-casamento/listas-casamento`,
    }
};
