import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import {
  StorageHomeVendedor,
  StorageIntegracaoService,
  AtividadeExecutorService,
  Atividade,
  SelecionarClienteSolicitacao
} from 'vv-jornada-core-ngx/dist/integracao';
import { ListaCasamentoService } from './../shared/services/lista-casamento.service';
import { Atendimento } from 'vv-jornada-core-ngx/dist/solicitar-atendimento';

@Component({
  selector: 'vnd-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  public funcionario: StorageHomeVendedor;
  private subscriptions = new Subscription();

  constructor(
    private storage: StorageIntegracaoService,
    private listaCasamentoService: ListaCasamentoService,
    private atividadeExecutorService: AtividadeExecutorService,
  ) { }

  ngOnInit() {
    this.applyActivities();
    this.applyEmployee();
  }

  private applyActivities(): void {
    this.selectRegistrationActivity();
  }

  private applyEmployee(): void {
    this.storage.lerLogin().then(login => this.funcionario = login.vendedor);
  }

  private selectRegistrationActivity(): void {
     const subscription = this.atividadeExecutorService
      .escutarResposta<Atendimento, SelecionarClienteSolicitacao>(Atividade.SELECIONAR_CADASTRO)
      .subscribe(() => this.criarLista());

      this.subscriptions.add(subscription);
  }

  public criarLista(): void {
    this.listaCasamentoService.criarListaCasamento();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
