import { RouterModule, Routes, CanActivateChild } from '@angular/router';
import { NgModule } from '@angular/core';
import { CadastroGuardService } from './cadastro/shared/services/cadastro-guard.service';
import {
   CadastroTelaSucessoComponent } from './cadastro/scenes/cadastro-confirmacao/cadastro-tela-sucesso/cadastro-tela-sucesso.component';

import { AuthGuardService } from 'vv-jornada-core-ngx/dist/integracao';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [AuthGuardService]
  },
  {
    path: 'cadastro',
    loadChildren: './cadastro/cadastro.module#CadastroModule',
    canActivateChild: [CadastroGuardService],
    canActivate: [AuthGuardService]
  },
  {
    path: 'sucesso',
    component: CadastroTelaSucessoComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      useHash: true,
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule {}
