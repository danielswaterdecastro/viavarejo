import { Component } from '@angular/core';

@Component({
  selector: 'vnd-root',
  template: `
    <vnd-header></vnd-header>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {

}
