# Projeto Jornada Lista de Casamento

## Servidor para desenvolvimento

Utilize o comando `ng serve` para iniciar o servidor de desenvolvimento. Este comando irá abrir no seu navegador padrão a url `http://localhost:4200/`. A aplicação irá se atualizar sozinha cada vez que salvar alguma alteração no código.

## Utilização do Angular CLI

Execute `ng generate component component-name` para criar um novo componente. É possível utilizar também `ng generate directive|pipe|service|class|guard|interface|enum|module`. 

Em caso de dúvidas consulte [a documentação](https://github.com/angular/angular-cli/wiki/generate).

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
