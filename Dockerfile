FROM nginx:stable

LABEL maintainer = "maiconpeixinhno.viavarejo.com.br"

RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx

RUN sed -i.bak 's/listen\(.*\)80;/listen 8081;/' /etc/nginx/conf.d/default.conf

EXPOSE 8081

RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf

ARG dist

COPY entrypoint.sh /entrypoint.sh

RUN chmod g+rwx /entrypoint.sh

COPY $dist /usr/share/nginx/html/lista-casamento/

ENTRYPOINT ["/entrypoint.sh"]

CMD nginx -g 'daemon off;'
